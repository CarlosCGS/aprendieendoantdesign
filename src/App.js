import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './index.css';
import { Button, message } from 'antd';
import Tarjeta from './components/Tarjeta';

function App() {
  const key = 'updatable';

  const openMessage = () => {
    message.loading({ content: 'Loading...', key });
    setTimeout(() => {
      message.success({ content: 'Loaded!', key, duration: 2 });
    }, 1000);
  };
  return (
    <>
    <Button type="primary" onClick={openMessage}>
      Open the message box
  </Button>
  <Tarjeta></Tarjeta>
  </>
  )
}




export default App;
